﻿// See https://aka.ms/new-console-template for more information
using structuredemo;

prodcut productobj = new prodcut();
customer customer = new customer(1 ,"cust1", 2);
productobj.productid = 10;
productobj.productname = "Dotnet";
productobj.cust1 = customer;
productobj.cust2 = new customer(2, "cust2", 3);
Console.WriteLine($" product Id :: {productobj.productid}\t productname::{productobj.productname} customer id:: {productobj.cust1.Id} ");
