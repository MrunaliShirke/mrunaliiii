﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace structuredemo
{
    struct customer
    {
        public int Id;
        public string Name;
        public int quantity;

        public customer(int id, string name, int quant)
        {
            this.Id = id;
            this.Name = name;
            this.quantity = quant;
        }
    }
}
