﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace day3_classarraytask.model
{
    internal class modelclass
    {
        
        
            public string name { get; set; }
            public string category { get; set; }
            public int price { get; set; }
            public float rating { get; set; }
            public modelclass(string name, string category, int price, float rating)
            {
                this.name = name;
                this.category = category;
                this.price = price;
                this.rating = rating;
            }
        }
    }


