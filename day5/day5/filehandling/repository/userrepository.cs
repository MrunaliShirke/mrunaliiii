﻿using filehandling.Exception;
using filehandling.model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace filehandling.repository
{
    internal class userrepository : Iuserinterface1,Ifile
    {
        List<userclass> user;
        //constructory
        public userrepository()
        {

            user = new List<userclass>();   
        }

        

        public bool Register(userclass userclass)
        {
            user.Add(userclass);
            string filename = "userdatabase.txt";
            bool result = Isusernameexist(userclass.Name, filename);
          if (result) 
            {
                return false;
            }
            else
            {
                Writecontent(userclass, filename);
                Console.WriteLine("Register successfully");
                return true;
            }
            
        }

        public void Writecontent(userclass user, string filename)
        {
           using (StreamWriter sw = new StreamWriter(filename,true))
            {
                sw.Write($"{user}");
            }

        }
        

        

        public bool Isusernameexist(string username, string filename)
        {
            bool isUseravilable = false;
            using (StreamReader sr = new StreamReader(filename))
            {
                string rowline;
                while ((rowline = sr.ReadLine()) != null)
                {
                    string[] rowSlitted = rowline.Split(',');
                    foreach(string item in rowSlitted)
                    {
                        if(item == username)
                        {
                            isUseravilable = true;
                            throw new Invaliduser("Username already exist");
                  
                            break;
                        }
                    }
                }
            }return isUseravilable;
        }

        public List<string> Readcontentfromfile(string filename)
        {
            List<string> filecontent = new List<string>();
            using (StreamReader sr = new StreamReader(filename))
            {
                string rowLine;
                while ((rowLine = sr.ReadLine()) != null)
                {
                    filecontent.Add(rowLine);

                }
                return filecontent;
            }

        }
    }
}
