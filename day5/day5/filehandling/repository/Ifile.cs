﻿using filehandling.model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace filehandling.repository
{
    internal interface Ifile
    {
        void Writecontent(userclass user, string filename);
        List<string> Readcontentfromfile( string filename);

         bool Isusernameexist(string username, string filename);

    }
}
