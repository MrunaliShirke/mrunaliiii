﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace filehandling.model
{
    internal class userclass
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string city { get; set; }

        public override string ToString()
        {
            return ($"{Id} ,{Name}  ,{city}\n");
        }
    }
}
