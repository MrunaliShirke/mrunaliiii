﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace classarray.model
{
    internal class product
    {   public int Id { get; set; }
        public string name { get; set; }
        public string category { get; set; }
        public int price { get; set; }
        public float rating { get; set; }
        public product( int id,string name, string category, int price, float rating)
        {
            this.Id = id;
            this.name = name;
            this.category = category;
            this.price = price;
            this.rating = rating;
        }
        public override string ToString()
        {
            return $" Id::{Id} \t name ::{name} category : : {category} price :: {price}  rating ::{rating} "
;        }
    }
}
