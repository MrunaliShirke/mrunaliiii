﻿//mobile no should be 10 digit
//should includ country code , 91
//+sign may be preset or maynot be
//first digit of mobile number should be 6-9
//rest of digit should be between 0-9

using System.Text.RegularExpressions;

public static class validate
{
    public const string pattern = @"^[+]?91[-\s]?[6-9][0-9]{9}$";

    public static bool validatemob(string num)
    {
        if (num != null) return Regex.IsMatch(num, pattern);
        else return false;
    }


}

public class program
{
    static void Main()
    {
        Console.WriteLine("Enter your no");
        string mobnum = Console.ReadLine();
        Console.WriteLine(validate.validatemob(mobnum));
    }
}