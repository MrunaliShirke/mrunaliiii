﻿

using System.Text.RegularExpressions;

public static class validate
{
    //public const string pattern = @"^[+]?91[-\s]?[6-9][0-9]{9}$";
    public const string pattern = "^(?=.*?[A-Z])(?=.*?[a-z])(?=.*?[0-9])(?=.*?[!@#$]).{8}$";

    public static bool validatepass(string pass)
    {
        if (pass != null) return Regex.IsMatch(pass, pattern);
        else return false;
    }


}

public class program
{
    static void Main()
    {
        Console.WriteLine("Enter your password");
        string pass = Console.ReadLine();
        Console.WriteLine("**************************************");
        Console.WriteLine(validate.validatepass(pass));
    }
}