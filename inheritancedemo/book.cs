﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace inheritancedemo
{
    internal class book : product
    {
        public int Price { get; set; }
        public book( int Price,String Name, int quantity) : base(Name, quantity)
        { 
            this.Price = Price;
        }
        public override void Addtocart()
        {
            int discount = 1000;
            double carttotal = (Price * quantity) - discount;
            Console.WriteLine($"Your carttotal is::{discount}");
            
        }

        public override void BookDetails()
        {
            Console.WriteLine($"Book Name ::{Name} Book price :;{Price} Qunatity you booked::{quantity}");
        }
        public string Bookorigin()
        {
            return "this is book class method";
        }
    }
}
