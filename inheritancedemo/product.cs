﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace inheritancedemo
{
    abstract class product
    {
        public string Name { get; set; }
        public int quantity { get; set; }
        public product(string Name, int Idquantity)
        {    
            this.Name = Name;
            this.quantity = quantity;   

        }
        public abstract void BookDetails();
        public abstract void Addtocart();
        
    }
}
